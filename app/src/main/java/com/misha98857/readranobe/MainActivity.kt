package com.misha98857.readranobe

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val navController = Navigation.findNavController(this, R.id.container)

        when (item.itemId) {
            R.id.navigation_home -> {
                navController.navigate(R.id.firstScreenFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                navController.navigate(R.id.searchFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_star -> {
                navController.navigate(R.id.starFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_settings -> {
                navController.navigate(R.id.settingFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    fun ErrorInternet() {

    }
}
