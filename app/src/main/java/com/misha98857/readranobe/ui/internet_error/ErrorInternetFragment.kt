/*
 * Copyright (c) 2019. created misha98857.
 */

package com.misha98857.readranobe.ui.internet_error

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.misha98857.readranobe.MainActivity
import com.misha98857.readranobe.R
import kotlinx.android.synthetic.main.error_internet_fragment.*

class ErrorInternetFragment : Fragment() {

    companion object {
        fun newInstance() = ErrorInternetFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.error_internet_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        reload_button.setOnClickListener {
            val navController = Navigation.findNavController(activity as MainActivity, R.id.container)
            navController.navigate(R.id.firstScreenFragment)
        }
    }

}
