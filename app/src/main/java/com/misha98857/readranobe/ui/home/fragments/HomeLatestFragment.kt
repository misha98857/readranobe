/*
 * Copyright (c) 2019. created misha98857.
 */

package com.misha98857.readranobe.ui.home.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.misha98857.readranobe.R
import com.misha98857.readranobe.ui.home.adapters.LatestRecyclerViewAdapter
import com.misha98857.readranobe.ui.home.adapters.PopularRecyclerViewAdapter
import com.misha98857.readranobe.utils.RanobeCardListLatest
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.uiThread


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [HomeLatestFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 *
 */
class HomeLatestFragment : Fragment() {
    val popularmImageUrls = arrayListOf<String>()
    val popularmNames = arrayListOf<String>()
    var result = JsonArray()
    companion object {
        fun newInstance() = HomePopularFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_latest, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        doAsync {
            val result1 = arguments?.getString("result").toString()
            val gson = GsonBuilder().create()
            //result = gson.fromJson(result1, JsonObject::class.java) as JsonObject
            result = gson.fromJson(result1, JsonArray::class.java) as JsonArray
            uiThread { getImages(result) }
        }
    }

    fun getImages(result: JsonArray) {
        for (i in result) {
            popularmNames.add(i.asJsonObject["title"].toString())
            popularmImageUrls.add(i.asJsonObject["image"].asJsonObject["desktop"].asJsonObject["image"].toString())
        }
        latestRecyclerView()
    }

    private fun latestRecyclerView() {
        //val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val layoutManager = GridLayoutManager(context, 3)
        val recyclerView = find<RecyclerView>(R.id.latest_fragment)
        recyclerView.layoutManager = layoutManager
        recyclerView.isNestedScrollingEnabled = false
        val adapter = LatestRecyclerViewAdapter(this.context!!, popularmNames, popularmImageUrls)
        recyclerView.adapter = adapter
    }
}
