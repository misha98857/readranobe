/*
 * Copyright (c) 2019. created misha98857.
 */

package com.misha98857.readranobe.ui.first_screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.gson.JsonObject
import com.misha98857.readranobe.MainActivity
import com.misha98857.readranobe.R
import com.misha98857.readranobe.ui.home.home_utils.popularLoad
import com.misha98857.readranobe.utils.RanobeCardList
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync

class FirstScreenFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.first_screen_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val navController = Navigation.findNavController(activity as MainActivity, R.id.container)
        navController.navigate(R.id.homeFragment)
    }
}
