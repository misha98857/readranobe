/*
 * Copyright (c) 2019. created misha98857.
 */

package com.misha98857.readranobe.ui.loading

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.misha98857.readranobe.R
import kotlinx.android.synthetic.main.loading_fragment.*
import org.jetbrains.anko.image

class LoadingFragment : Fragment() {

    companion object {
        fun newInstance() = LoadingFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.loading_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val circularProgressDrawable = CircularProgressDrawable(this.context!!)
        circularProgressDrawable.strokeWidth = 10f
        circularProgressDrawable.centerRadius = 60f
        circularProgressDrawable.start()
        sync.image = circularProgressDrawable
    }

}
