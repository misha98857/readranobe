/*
 * Copyright (c) 2019. created misha98857.
 */

package com.misha98857.readranobe.ui.home.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.misha98857.readranobe.R
import com.misha98857.readranobe.ui.home.adapters.PopularRecyclerViewAdapter
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.uiThread

class HomePopularFragment : Fragment() {
    val popularmImageUrls = arrayListOf<String>()
    val popularmNames = arrayListOf<String>()
    var result = arrayListOf<JsonObject>()

    companion object {
        fun newInstance() = HomePopularFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.home_popular_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        doAsync {
            val result1 = arguments?.getString("result").toString()
            val gson = GsonBuilder().create()
            result = gson.fromJson(result1, Array<JsonObject>::class.java).toList() as ArrayList<JsonObject>
            uiThread { getImages(result) }
        }


    }

    fun getImages(result: ArrayList<JsonObject>) {
        for (i in result) {
            popularmNames.add(i["title"].toString())
            popularmImageUrls.add(i["image"].asJsonObject["desktop"].asJsonObject["image"].toString())
        }
        popularRecyclerView()
    }

    private fun popularRecyclerView() {
        //val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val layoutManager = GridLayoutManager(context, 3)
        val recyclerView = find<RecyclerView>(R.id.popular_fragment)
        recyclerView.layoutManager = layoutManager
        recyclerView.isNestedScrollingEnabled = false
        val adapter = PopularRecyclerViewAdapter(this.context!!, popularmNames, popularmImageUrls)
        recyclerView.adapter = adapter
    }
}
