/*
 * Copyright (c) 2019. created misha98857.
 */

package com.misha98857.readranobe.ui.home.home_utils

import android.app.Activity
import android.content.Context
import androidx.navigation.Navigation
import com.google.gson.JsonObject
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.misha98857.readranobe.R
import com.misha98857.readranobe.utils.RanobeCardList
import com.misha98857.readranobe.utils.RanobeCardListLatest
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface RanobeLoader {
    @FormUrlEncoded
    @POST("v1/book/popular/")
    fun popularRanobe(@Field("tab") tab: String): Observable<RanobeCardList>

    @FormUrlEncoded
    @POST("v1/book/last/")
    fun latestRanobe(@Field("page") page: Int, @Field("sequence") sequence: List<JsonObject>): Observable<RanobeCardListLatest>
}

private val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
    this.level = HttpLoggingInterceptor.Level.BODY
}

private val client: OkHttpClient = OkHttpClient.Builder().apply {
    this.addInterceptor(interceptor)
}.build()

private val retrofit: Retrofit =
    Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).baseUrl("https://xn--80ac9aeh6f.xn--p1ai/")
        .client(client)
        .build()

private val SERVICE: RanobeLoader = retrofit.create(
    RanobeLoader::class.java
)

fun popularLoad(): Observable<RanobeCardList> {
    val data = SERVICE.popularRanobe("alltime")
    return data
}

fun latestload(page : Int, sequence : List<JsonObject>): Observable<RanobeCardListLatest> {
    val data = SERVICE.latestRanobe(page, sequence)
    return data
}

// val popular: Call<RanobeCardList> = SERVICE.latestRanobe(page, sequence)
