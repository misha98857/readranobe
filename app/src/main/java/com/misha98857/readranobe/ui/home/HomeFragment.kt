/*
 * Copyright (c) 2019. created misha98857.
 */

package com.misha98857.readranobe.ui.home

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.misha98857.readranobe.MainActivity
import com.misha98857.readranobe.R
import com.misha98857.readranobe.ui.home.adapters.LatestRecyclerViewAdapter
import com.misha98857.readranobe.ui.home.adapters.PopularRecyclerViewAdapter
import com.misha98857.readranobe.ui.home.fragments.HomeLatestFragment
import com.misha98857.readranobe.ui.home.fragments.HomePopularFragment
import com.misha98857.readranobe.ui.home.home_utils.latestload
import com.misha98857.readranobe.ui.home.home_utils.popularLoad
import com.misha98857.readranobe.ui.internet_error.ErrorInternetFragment
import com.misha98857.readranobe.ui.loading.LoadingFragment
import com.misha98857.readranobe.utils.RanobeCardList
import com.misha98857.readranobe.utils.RanobeCardListLatest
import com.misha98857.readranobe.utils.RanobeCart
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.runOnUiThread
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.support.v4.uiThread
import org.jetbrains.anko.uiThread
import kotlin.collections.ArrayList


class HomeFragment : Fragment() {
    var result = arrayListOf<JsonObject>()
    var latest_result = JsonArray()

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        doAsync {
            popular_loader()
            latest_loader()
        }

        //val navController = Navigation.findNavController(activity as MainActivity, R.id.popular_container)
        //navController.navigate(R.id.homePopularFragment, arguments)
    }

    fun popular_loader() {
        val popularRanobe = popularLoad()
        //val navController = Navigation.findNavController(activity as MainActivity, R.id.container)
        popularRanobe.subscribeOn(Schedulers.computation()).observeOn(Schedulers.io())
            .subscribe(object : Observer<RanobeCardList> {
                override fun onSubscribe(d: Disposable) {
                    //navController.navigate(R.id.loadingFragment)
                    val loadingFragment = LoadingFragment.newInstance()
                    childFragmentManager.beginTransaction().add(R.id.popularview, loadingFragment)
                        .commitAllowingStateLoss()
                }

                override fun onNext(t: RanobeCardList) {
                    result = t.result
                    //HomeFragment.newInstance().result = result
                    val homepopularFragment = HomePopularFragment.newInstance()
                    homepopularFragment.arguments = bundleOf("result" to result.toString())
                    childFragmentManager.beginTransaction().add(R.id.popularview, homepopularFragment)
                        .commitAllowingStateLoss()
                    //val bundle = bundleOf("result" to result.toString())
                    //navController.navigate(R.id.homeFragment, bundle)
                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {
                    val navController = Navigation.findNavController(activity as MainActivity, R.id.container)
                    //navController.navigate(R.id.errorInternetFragment)
                    //childFragmentManager.beginTransaction().add(R.id.popularview, errorFragment).commit()
                }
            })
    }

    fun latest_loader() {
        val latestRanobe = latestload(1, arrayListOf<JsonObject>())
        //val navController = Navigation.findNavController(activity as MainActivity, R.id.container)
        latestRanobe.subscribeOn(Schedulers.computation()).observeOn(Schedulers.io())
            .subscribe(object : Observer<RanobeCardListLatest> {
                override fun onSubscribe(d: Disposable) {
                    //navController.navigate(R.id.loadingFragment)
                    val loadingFragment = LoadingFragment.newInstance()
                    childFragmentManager.beginTransaction().add(R.id.latestview, loadingFragment)
                        .commitAllowingStateLoss()
                }

                override fun onNext(t: RanobeCardListLatest) {
                    latest_result = t.result.asJsonObject["books"].asJsonArray
                    val homelatestFragment = HomeLatestFragment.newInstance()
                    homelatestFragment.arguments = bundleOf("result" to latest_result.toString())
                    childFragmentManager.beginTransaction().add(R.id.latestview, homelatestFragment)
                        .commitAllowingStateLoss()
                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {
                    val navController = Navigation.findNavController(activity as MainActivity, R.id.container)
                    navController.navigate(R.id.errorInternetFragment)
                    //childFragmentManager.beginTransaction().add(R.id.popularview, errorFragment).commit()
                }
            })
    }
}


//TODO при загрузке отправлять на фрагмент с загрузкой
//TODO у кнопки сделать перенаправление на фрагмент, который был до этого
//TODO сохранение состояния фрагмента
