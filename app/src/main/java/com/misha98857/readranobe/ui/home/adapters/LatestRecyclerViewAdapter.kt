/*
 * Copyright (c) 2019. created misha98857.
 */

package com.misha98857.readranobe.ui.home.adapters


import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.misha98857.readranobe.R
import com.misha98857.readranobe.utils.GlideApp
import java.util.*


class LatestRecyclerViewAdapter(
    private val mContext: Context,
    names: ArrayList<String>,
    imageUrls: ArrayList<String>
) :
    RecyclerView.Adapter<LatestRecyclerViewAdapter.ViewHolder>() {
    //vars
    private val mNames = names
    private val mImageUrls = imageUrls

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.ranobe_listitem, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.i("LOAD IMAGE", "$position")
        val circularProgressDrawable = CircularProgressDrawable(mContext)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()
        val image_url = mImageUrls[position].replace("\"", "")


        Log.i("IMAGE HREF", image_url)
        GlideApp.with(mContext).asBitmap()
            .placeholder(circularProgressDrawable)
            //.load(popularmImageUrls[position]).centerCrop()
            .load(image_url)
            .into(holder.image)

        holder.name.text = mNames[position]

        holder.image.setOnClickListener {
            Log.d("Ranobe_Name", "onClick: clicked on an image: " + mNames[position])
            Toast.makeText(mContext, mNames[position], Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return mImageUrls.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var image: ImageView = itemView.findViewById(R.id.image_view)
        internal var name: TextView = itemView.findViewById(R.id.name)

    }

    companion object {

        private val TAG = "LatestRecyclerViewAdapter"
    }

}
