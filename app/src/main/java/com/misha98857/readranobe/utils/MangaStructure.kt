/*
 * Copyright (c) 2019. created misha98857.
 */

package com.misha98857.readranobe.utils

import com.google.gson.JsonObject

data class RanobeCardList (val message : String,
                           val result: ArrayList<JsonObject>,
                           val status: Int)

data class RanobeCardListLatest (val message : String,
                           val result: JsonObject,
                           val status: Int)

data class RanobeCart (val id : Int,
                       val url : String,
                       val title: String,
                       val view: Int,
                       val dislikes: Int,
                       val image: JsonObject,
                       val likes: Int,
                       val part: JsonObject,
                       val user: JsonObject)


